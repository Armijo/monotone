#ifndef SETTINGS_H
#define SETTINGS_H

#include <QSettings>
#include <QMap>


class Settings
{
public:
    Settings(const QString &org, const QString &app, QObject *parent = 0);
    ~Settings();

    void setValue(const QString&, const QVariant&);
    QVariant value(const QString&, const QVariant& = QVariant());
    void reset();
    void flush();

private:
    QMap<QString, QVariant> prefs;
    QSettings settings;
};

#endif // SETTINGS_H
