#-------------------------------------------------
#
# Project created by QtCreator 2015-06-21T12:59:35
#
#-------------------------------------------------

QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = msg
TEMPLATE = app


SOURCES += main.cpp\
        mainwindow.cpp \
    diasettings.cpp \
    settings.cpp

HEADERS  += mainwindow.h \
    diasettings.h \
    settings.h

FORMS    += mainwindow.ui \
    diasettings.ui
