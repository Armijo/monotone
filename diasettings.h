#ifndef SETTINGS_H
#define SETTINGS_H

#include <QDialog>

namespace Ui {
class Settings;
}

class DiaSettings : public QDialog
{
    Q_OBJECT

public:
    explicit DiaSettings(QWidget *parent = 0);
    ~DiaSettings();

private:
    Ui::Settings *ui;
};

#endif // SETTINGS_H
