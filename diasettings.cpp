#include "diasettings.h"
#include "ui_diasettings.h"

DiaSettings::DiaSettings(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::Settings)
{
    ui->setupUi(this);
}

DiaSettings::~DiaSettings()
{
    delete ui;
}
