#include "settings.h"

Settings::Settings(const QString &org, const QString &app, QObject *parent) : settings(org, app, parent)
{

}

Settings::~Settings()
{

}

void Settings::setValue(const QString &key, const QVariant &value)
{
    this->prefs[key] = value;
}

QVariant Settings::value(const QString &key, const QVariant &def)
{
    if(!this->prefs.contains(key))
    {
        return this->settings.value(key, def);
    }
    else
    {
        return this->prefs[key];
    }
}

void Settings::reset()
{
    this->prefs.clear();
}

void Settings::flush()
{
    for(QMap<QString, QVariant>::iterator iter = this->prefs.begin(); iter != this->prefs.end(); ++iter)
    {
        this->settings.setValue(iter.key(), iter.value());
    }
    this->reset();
}
